# Fronius Exporter
Prometheus metric exporter for Fronius inverters.

## Usage
Start the exporter web server by providing the URL to the Fronius inverter:

```bash
./fronius-exporter http://192.168.1.5/
```

By default the web server listens on port 9173:

```bash
curl http://localhost:9173/metrics
```


