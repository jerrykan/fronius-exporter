METRIC_DOCS = {
    'fronius_inverter_energy_day_watt_hours': {
        'help': 'AC Energy generated on current day (Wh)',
        'type': 'counter',
    },
    'fronius_inverter_energy_year_watt_hours': {
        'help': 'AC Energy generated in current year (Wh)',
        'type': 'counter',
    },
    'fronius_inverter_energy_total_watt_hours': {
        'help': 'AC Energy generated overall (Wh)',
        'type': 'counter',
    },
    'fronius_inverter_fac_hertz': {
        'help': 'AC frequency (Hz)',
        'type': 'gauge',
    },
    'fronius_inverter_iac_amperes': {
        'help': 'AC current (A) (absolute, accumulated over all lines)',
        'type': 'gauge',
    },
    'fronius_inverter_idc_amperes': {
        'help': 'DC current (A)',
        'type': 'gauge',
    },
    'fronius_inverter_pac_watts': {
        'help': 'AC power (W) (negative value for consuming power)',
        'type': 'gauge',
    },
    'fronius_inverter_uac_volts': {
        'help': 'AC voltage (V)',
        'type': 'gauge',
    },
    'fronius_inverter_udc_volts': {
        'help': 'DC voltage (V)',
        'type': 'gauge',
    },
    'fronius_inverter_pmax_day_watts': {
        'help': 'Maximum AC power of current day (W)',
        'type': 'gauge',
    },
    'fronius_inverter_pmax_year_watts': {
        'help': 'Maximum AC power of current year (W)',
        'type': 'gauge',
    },
    'fronius_inverter_pmax_total_watts': {
        'help': 'Maximum AC power overall (W)',
        'type': 'gauge',
    },
    'fronius_inverter_uacmax_day_volts': {
        'help': 'Maximum AC voltage of current day (V)',
        'type': 'gauge',
    },
    'fronius_inverter_uacmax_year_volts': {
        'help': 'Maximum AC voltage of current year (V)',
        'type': 'gauge',
    },
    'fronius_inverter_uacmax_total_volts': {
        'help': 'Maximum AC voltage overall (V)',
        'type': 'gauge',
    },
    'fronius_inverter_udcmax_day_volts': {
        'help': 'Maximum DC voltage of current day (V)',
        'type': 'gauge',
    },
    'fronius_inverter_udcmax_year_volts': {
        'help': 'Maximum DC voltage of current year (V)',
        'type': 'gauge',
    },
    'fronius_inverter_udcmax_total_volts': {
        'help': 'Maximum DC voltage overall (V)',
        'type': 'gauge',
    },
}
