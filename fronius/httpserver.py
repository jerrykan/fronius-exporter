from http.server import HTTPServer, BaseHTTPRequestHandler

from . import inverter, prometheus


class MetricsHandler(BaseHTTPRequestHandler):
    def _default_response(self):
        content = [
            '<html>',
            '<head><title>Fronius Inverter Exporter</title></head>',
            '<body>',
            '<h1>Fronius Inverter Exporter</h1>'
            '<p><a href="/metrics">Metrics</a></p>'
            '</body>',
            '</html>',
        ]

        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        self.wfile.write('\n'.join(content).encode('utf-8'))

    def _metrics_response(self):
        data = inverter.get_data(
            self.server.inverter_url,
            self.server.device_ids,
        )
        response = prometheus.formatter(data)

        self.send_response(200)
        self.send_header('Content-type', 'text/plain')
        self.end_headers()
        self.wfile.write(response.encode('utf-8'))

    def do_GET(self):
        if self.path == '/metrics':
            self._metrics_response()
            return

        self._default_response()


class MetricsServer(HTTPServer):
    def __init__(self, inverter_url, interface='', port=8000, device_ids=None):
        self.inverter_url = inverter_url.rstrip('/')

        if device_ids:
            self.device_ids = device_ids
        else:
            self.device_ids = inverter.get_device_ids(self.inverter_url)

        server_address = (interface, port)
        super().__init__(server_address, MetricsHandler)

    def serve_forever(self):
        print('Listening on {}:{}'.format(*self.server_address))
        super().serve_forever()
