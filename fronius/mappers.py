def realtime_common_inverter_data(data):
    ret_data = {}

    for src, dst in (
        # Energy
        ('DAY_ENERGY', 'fronius_inverter_energy_day_watt_hours'),
        ('YEAR_ENERGY', 'fronius_inverter_energy_year_watt_hours'),
        ('TOTAL_ENERGY', 'fronius_inverter_energy_total_watt_hours'),
        # AC
        ('FAC', 'fronius_inverter_fac_hertz'),
        ('IAC', 'fronius_inverter_iac_amperes'),
        ('PAC', 'fronius_inverter_pac_watts'),
        ('UAC', 'fronius_inverter_uac_volts'),
        # DC
        ('IDC', 'fronius_inverter_idc_amperes'),
        ('UDC', 'fronius_inverter_udc_volts'),
    ):
        try:
            ret_data[dst] = data[src]['Value']
        except KeyError:
            pass

    return ret_data


def realtime_min_max_inverter_data(data):
    ret_data = {}

    for src, dst in (
        # Day maximums
        ('DAY_PMAX', 'fronius_inverter_pmax_day_watts'),
        ('DAY_UACMAX', 'fronius_inverter_uacmax_day_volts'),
        ('DAY_UDCMAX', 'fronius_inverter_udcmax_day_volts'),
        # Year maximums
        ('YEAR_PMAX', 'fronius_inverter_pmax_year_watts'),
        ('YEAR_UACMAX', 'fronius_inverter_uacmax_year_volts'),
        ('YEAR_UDCMAX', 'fronius_inverter_udcmax_year_volts'),
        # Total maximums
        ('TOTAL_PMAX', 'fronius_inverter_pmax_total_watts'),
        ('TOTAL_UACMAX', 'fronius_inverter_uacmax_total_volts'),
        ('TOTAL_UDCMAX', 'fronius_inverter_udcmax_total_volts'),
    ):
        try:
            ret_data[dst] = data[src]['Value']
        except KeyError:
            pass

    return ret_data
