import logging
from urllib.error import URLError
from urllib.request import urlopen

from . import METRIC_DOCS

DEFAULT_TIMEOUT = 5

_logger = logging.getLogger(__name__)


def formatter(data, comments=True):
    output = []
    for device, metrics in sorted(data['devices'].items()):
        for metric, value in sorted(metrics.items()):
            if comments:
                output.append(f"# HELP {metric} {METRIC_DOCS[metric]['help']}")
                output.append(f"# TYPE {metric} {METRIC_DOCS[metric]['type']}")
            output.append(f'{metric}{{device="{device}"}} {value}')

    return '\n'.join(output)


def push(data, url, timeout=DEFAULT_TIMEOUT):
    post_data = formatter(data, comments=False).encode()

    try:
        urlopen(url, data=post_data, timeout=timeout)
    except URLError as err:
        _logger.error(f'Unable to POST to URL {url}: {err}')
