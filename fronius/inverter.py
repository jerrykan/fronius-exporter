import json
import logging
from collections import defaultdict
from concurrent.futures import ThreadPoolExecutor
from urllib.error import URLError
from urllib.parse import urlencode
from urllib.request import urlopen

from . import mappers

DEFAULT_TIMEOUT = 5
POWERFLOW_ENDPOINT = '/solar_api/v1/GetPowerFlowRealtimeData.fcgi'
DATA_ENDPOINTS = [
    {
        'path': '/solar_api/v1/GetInverterRealtimeData.cgi',
        'params': {
            'Scope': 'Device',
            'DataCollection': 'CommonInverterData',
            'DeviceId': '{device}',
        },
        'mapper': mappers.realtime_common_inverter_data,
    },
    {
        'path': '/solar_api/v1/GetInverterRealtimeData.cgi',
        'params': {
            'Scope': 'Device',
            'DataCollection': 'MinMaxInverterData',
            'DeviceId': '{device}',
        },
        'mapper': mappers.realtime_min_max_inverter_data,
    },
]

_logger = logging.getLogger(__name__)


def fetch_json(url, params=None, timeout=DEFAULT_TIMEOUT):
    if params:
        url = '{}?{}'.format(url, urlencode(params))

    try:
        response = urlopen(url, timeout=timeout)
    except URLError as err:
        log = _logger.warning

        if isinstance(getattr(err, 'reason', None), TimeoutError):
            log = _logger.info

        log(f'Unable to fetch URL {url}: {err}')
        return {}

    try:
        json_data = json.load(response.fp)
    except json.JSONDecodeError:
        _logger.warning(f'Unable to decode JSON from URL: {url}')
        return {}

    return json_data.get('Body', {}).get('Data', {})


def get_device_ids(inverter_url, timeout=DEFAULT_TIMEOUT):
    inverter_url = inverter_url.rstrip('/')
    response = fetch_json(f'{inverter_url}{POWERFLOW_ENDPOINT}')

    return list(response.get('Inverters', {}).keys())


def get_endpoint_data(
    inverter_url,
    endpoint,
    device_id,
    timeout=DEFAULT_TIMEOUT,
):
    params = endpoint.get('params', {})

    if 'DeviceId' in params:
        params = params.copy()
        params['DeviceId'] = device_id

    return fetch_json(
        f'{inverter_url}{endpoint["path"]}',
        params=params,
        timeout=timeout,
    )


def get_data(inverter_url, device_ids):
    inverter_url = inverter_url.rstrip('/')
    data = {'devices': defaultdict(dict)}
    futures = []

    with ThreadPoolExecutor() as pool_exec:
        for device_id in device_ids:
            for endpoint in DATA_ENDPOINTS:
                future_response = pool_exec.submit(
                    get_endpoint_data,
                    inverter_url,
                    endpoint,
                    device_id,
                )

                futures.append([device_id, endpoint, future_response])

    for device_id, endpoint, future in futures:
        response = future.result()

        if not response:
            continue

        mapped_data = endpoint['mapper'](response)

        data['devices'][device_id].update(mapped_data)

    return data
